package sqlite.douglazsilva.tk.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("app", MODE_PRIVATE, null);

            //criar tabela
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS pessoas(id INTEGER PRIMARY KEY AUTOINCREMENT, nome VARCHAR, idade INT(3))");

            //apagar tabela
            //sqLiteDatabase.execSQL("DROP TABLE pessoas");

            //inserir dados
            //sqLiteDatabase.execSQL("INSERT INTO pessoas(nome, idade) VALUES('Mariana', 18)");
            //sqLiteDatabase.execSQL("INSERT INTO pessoas(nome, idade) VALUES('João', 50)");

            //atualizar dados
            sqLiteDatabase.execSQL("UPDATE pessoas SET nome = 'Marina' WHERE id = 1");

            //deletar dados
            //sqLiteDatabase.execSQL("DELETE FROM pessoas WHERE id = 1 ");

            //recuperar dados
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM pessoas", null);
            //Cursor cursor = sqLiteDatabase.rawQuery("SELECT nome, idade FROM pessoas WHERE idade > 20 AND nome = 'Marcos'", null);
            //Cursor cursor = sqLiteDatabase.rawQuery("SELECT nome, idade FROM pessoas WHERE nome LIKE '%ar%' ", null);
            int indiceColunaId = cursor.getColumnIndex("id");
            int indiceColunaNome = cursor.getColumnIndex("nome");
            int indiceColunaIdade = cursor.getColumnIndex("idade");

            cursor.moveToFirst();

            while (cursor != null) {
                Log.i("RESULTADO - id: ", cursor.getString(indiceColunaId));
                Log.i("RESULTADO - nome: ", cursor.getString(indiceColunaNome));
                Log.i("RESULTADO - idade: ", cursor.getString(indiceColunaIdade));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
